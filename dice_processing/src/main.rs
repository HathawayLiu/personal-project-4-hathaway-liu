use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use rand::Rng; // Include the rand crate for random number generation
use serde::{Deserialize, Serialize};
use tracing_subscriber;

#[derive(Deserialize)]
struct Request {
    req_id: String,
    payload: u32,
}

#[derive(Serialize)]
struct Response {
    roll_id: String,
    message: String,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Generate a random roll between 1 and 6
    let mut rng = rand::thread_rng();
    let my_roll = rng.gen_range(1..=6);
    let roll_id = event.payload.req_id;
    let user_roll = event.payload.payload;
    // Compare the random roll with the DynamoDB roll and prepare the message
    let message = if my_roll == user_roll {
        "We roll the same number".to_string()
    } else if my_roll > user_roll {
        "My roll is larger than yours".to_string()
    } else {
        "Your roll is larger than mine".to_string()
    };

    // Log the rolls and the outcome
    tracing::info!("Roll ID: {}, User Roll: {}, My Roll: {}, Message: {}", roll_id, user_roll, my_roll, message);

    // Prepare the response
    let resp = Response {
        roll_id,
        message,
    };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter("info")
        .with_target(false)  
        .without_time() 
        .init();

    run(service_fn(function_handler)).await
}