# Personal Project 4: Step Function workflow with DynamoDB
## Project Description
This individual project requires us to write multiple lambda functions in Rust and creates a step function workflow on AWS, where this step function orchestrate database and data processing pipeline

## Step Function logics
For this specific project, I create a database called `dice_rolls` on `DynamoDB` with some data processing:
1. `dice_db`: This lambda function creates a `dice_rolls` database on DynamoDB with the following data structure:
    ```rust
    roll_id: String,
    roll_value: u32
    ```
    and the output of the function will be:
    ```rust
    req_id: String,
    payload: u32
    ```
    corresponds the two keys in the database.
2. `dice_processing`: With the output of `dice_rolls`, the new function then rolls another dice called `user_rolls`, and the output will become `my_rolls`. The function then compares these two rolls and generate some message based on the result. The output of this function will be:
    ```rust
    roll_id: String,
    message: String
    ```
3. `dice_rolling_chain`: With the functions above, we could create a step function that use `cargo lambda invoke --data-ascii "{ \"command\": \"roll\"}"` to trigger `dice_db` and output the response of `dice_processing`.

## Detailed steps
1. Use multiple `cargo lambda new <YOUR PROJECT NAME>` to create multiple lambda functions (You could decide the number).
2. Create a main folder to store the functions you create above.
3. Implement those functions separately. Make sure that the output of the previous function should be the request of the next function so that your chain could be connected. Add the corresponding dependencies in `Cargo.toml` and `Makefile`. 
4. You could test your functions separately by `cargo lambda invoke --data-ascii "{ \"command\": "your command"}"` after `cargo lambda build`.
5. Go to `AWS IAM`. Create a new user with the following policies: `iamfullaccess`, `lambdafullaccess` and other policies you need. Then go to `Secruity Credentials` to create new access key. To make sure the function works, please use this user for all the functions you created.
6. Create a `.env` file and add your two access keys and access region to the file with the following structure:
    ```plaintext
    AWS_ACCESS_KEY_ID=
    AWS_SECRET_ACCESS_KEY=
    AWS_REGION=
    ```
    Then create a `.gitignore` file:
    ```plaintext
    /target
    .env
    ```
    Then use either `Export` or `set -a` and `source .env` to allow your terminal to detect your AWS user
7. Deploy your function separately. Then go to `AWS Lambda` to verify that the function exist in `Lambda`
8. Go to `AWS Step Functions`, select `Create State Machine`, then drag numbers of `lambdas invoke` function that match the number of functions that you created. Make sure they follow the sequence you designed. In the `Code` area, you could find the code of the step function and store it in a JSON file (Mine was `stepfunction.JSON`).
9. Give a name to the step function and create it. On the top right you could click on `Start Execution` and type in your own test case to test if it works. A successful step function should look like the following:
    ![](https://gitlab.com/HathawayLiu/personal-project-4-hathaway-liu/-/wikis/uploads/4c49b6770442a55fb497a9030b744719/Screenshot_2024-04-15_at_11.20.13_PM.png)

## Demo Video
![demo](https://gitlab.com/HathawayLiu/personal-project-4-hathaway-liu/-/wikis/uploads/affbceaaffcb0b7045981cca43112d38/video1474706052.mp4)
## Reference
[Rust AWS Step Functions](https://www.youtube.com/watch?v=2UktR8XSCE0)
