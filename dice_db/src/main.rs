use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use rand::Rng;
use serde::{Deserialize, Serialize};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};

// Define a struct to hold the dice rolling result
#[derive(Debug, Serialize, Deserialize)]
struct DiceRoll {
    roll_id: String,
    roll_value: u32,
}

#[derive(Deserialize)]
struct Request {
    command: String,
}

// Updated Response struct to include payload instead of msg
#[derive(Serialize)]
struct Response {
    req_id: String,
    payload: u32,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let roll_result = rand::thread_rng().gen_range(1..=6);
    let roll_id = event.context.request_id;
    let dice_roll = DiceRoll {
        roll_id: roll_id.clone(),
        roll_value: roll_result,
    };

    let config = aws_config::from_env().region("us-east-1").load().await;
    let client = Client::new(&config);

    let roll_id_attr = AttributeValue::S(dice_roll.roll_id.clone());
    let roll_value_attr = AttributeValue::N(dice_roll.roll_value.to_string());

    let table_name = "dice_rolls";

    let _resp = client
        .put_item()
        .table_name(table_name)
        .item("roll_id", roll_id_attr)
        .item("roll_value", roll_value_attr)
        .send()
        .await?;

    let resp = Response {
        req_id: roll_id,
        payload: roll_result,
    };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
